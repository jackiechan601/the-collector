#pragma once
#include <SFML/Graphics.hpp>
#include <vector>
class Item
{
public:
// Constructor
	Item(std::vector<sf::Texture> & itemTextures, sf::Vector2u screenSize);
	
	// Variables (data members) used by this class:
	sf::Sprite sprite;
	int pointsValue;
};

